<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;
use Auth;

class BlogPostController extends Controller
{
    public function index()
    {

       $posts = BlogPost::where('user_id', Auth::id())->get();

        return view('blog.index', [
            'posts' => $posts,
        ]); 
    }

    public function create()
    {
        return view('blog.create');
    }

   
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'body' => 'required',
        ],   
        [
            'title.required' => 'Post Title is Required',
            'body.required' => 'Post Body is Required'
        ]
        );

        $newPost = BlogPost::create([
            'title' => $request->title,
            'body' => $request->body,
            'user_id' => Auth::id()
        ]);

        return redirect('/blog');;
    }

    public function show(BlogPost $blogPost)
    {
        return view('blog.show', [
            'post' => $blogPost,
        ]);
    }

    
    public function edit(BlogPost $blogPost)
    {
   

        return view('blog.edit', [
            'post' => $blogPost,
        ]); 
    }

    
    public function update(Request $request, BlogPost $blogPost)
    {
        $validated = $request->validate([
            'title' => 'required',
            'body' => 'required',
        ],   
        [
            'title.required' => 'Post Title is Required',
            'body.required' => 'Post Body is Required'
        ]
        );

        $blogPost->update([
            'title' => $request->title,
            'body' => $request->body
        ]);

        return redirect('/blog');;
    }

    
    public function destroy(BlogPost $blogPost)
    {
        
        $blogPost->delete();
        return redirect('/blog');
    }
}
