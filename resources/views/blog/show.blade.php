@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-8 pt-2">
                <a href="/blog" class="btn btn-outline-primary btn-md mb-5">Go back</a>
            
                <h1 class="display-one">{{ ucfirst($post->title) }}</h1>
                <p>{!! $post->body !!}</p> 
                <hr>
                <a href="/blog/{{ $post->id }}/edit" class="btn btn-outline-primary">Edit Post</a>
                <br><br>
                <form id="delete-frm" class="" action="" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger" onclick="return (confirm('Are you sure you want to delete this blog?'))">Delete Post</button>
                </form>
            </div>
        </div>
    </div>
@endsection